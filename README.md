# terrarium-application

***Terrarium***

Application to control the hardware


## Getting Started for Developers

These instructions introduce how to run project on your local machine for development purposes for the first time

### Git Configuration

After cloning a repo first time use below command:

    for Windows user <code>git config --global core.autocrlf true</code>
    
    for Unix-based system user <code>git config --global core.autocrlf input</code>

### Built with

Download needed IDE:
* [Download JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Download Android Studio](https://developer.android.com/studio/?gclid=EAIaIQobChMI1p6y5Or-3wIViOmaCh1pxQJjEAAYASABEgLz0fD_BwE)
* Open and make project

Configure Android Studio:
* Open <code>SDK Manager</code> and dowload platforms at least up to Android 5.0
* To test project on real device download <code>Google USB Driver</code> in SDK Manager
* To test on emulator create virtual device in AVD Manager

If the application successfully runs skip next step:
(Windows)
* search for and go to <code>view advanced system settings</code> >> <code>Environment Variables</code>
* if there are no <code>JAVA_HOME</code>, so add this variable with value of path to your JDK folder
* add <code>%JAVA_HOME%\bin</code> to <code>Path</code>

***P.S.*** To develop a project create personal branch, add code there and then make pull request to '''develop''' branch